using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FPSVNG
{
    public class WeaponManager : MonoBehaviour
    {
        #region [ Fields ]

        [SerializeField] private PlayerInputHandler _inputHandler;
        [SerializeField] private List<WeaponHandler> _weapons = new List<WeaponHandler>();

        private int _currentEquipedIdx = 0;

        public WeaponHandler CurrentSelected => _weapons[_currentEquipedIdx];

        #endregion

        #region [ Unity Lifecycle ]

        private void Start()
        {
            if (_weapons.Count > 0)
            {
                foreach (var item in _weapons)
                {
                    item.gameObject.SetActive(false);
                }

                _currentEquipedIdx = 0;
                _weapons[_currentEquipedIdx].gameObject.SetActive(true);
            }
        }

        private void Update()
        {
            HandleSwitchWeapon();
        }

        #endregion

        #region [ Private Methods ]

        private void HandleSwitchWeapon()
        {
            var i = _inputHandler.GetSelectWeaponInput() - 1;
            if (i >= 0 && i < _weapons.Count && _currentEquipedIdx != i) 
            {
                _weapons[_currentEquipedIdx].gameObject.SetActive(false);
                _currentEquipedIdx = i;
                _weapons[_currentEquipedIdx].gameObject.SetActive(true);
            }
        }

        #endregion
    }
}