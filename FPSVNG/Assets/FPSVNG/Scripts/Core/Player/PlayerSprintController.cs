using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FPSVNG
{
    [RequireComponent(typeof(PlayerInputHandler),typeof(PlayerMovementController), typeof(PlayerCrunchController))]
    public class PlayerSprintController : MonoBehaviour
    {
        #region [ Fields ]

        [SerializeField] private float _sprintSpeed = 10f;
        [SerializeField] private float _walkSpeed = 5f;
        [SerializeField] private float _crunchSpeed = 2f;

        [SerializeField] private Animator _gunAnimator;

        private PlayerMovementController _movementController;
        private PlayerInputHandler _inputHandler;
        private PlayerCrunchController _crunchController;

        private bool _isSprinting;

        public bool IsSprinting => _isSprinting;

        #endregion

        #region [ Unity Lifecycle ]

        private void Start()
        {
            _movementController = GetComponent<PlayerMovementController>();
            _inputHandler = GetComponent<PlayerInputHandler>();
            _crunchController = GetComponent<PlayerCrunchController>();
        }

        private void Update()
        {
            HandleSprint();
        }

        #endregion

        #region [ Private Methods ]

        private void HandleSprint()
        {
            if (_crunchController.IsCrunch)
            {
                _isSprinting = false;
                _movementController.Speed = _crunchSpeed;
            }
            else if (_inputHandler.GetSprintInputHeld())
            {
                _isSprinting = true;
                _movementController.Speed = _sprintSpeed;
            }
            else
            {
                _isSprinting = false;
                _movementController.Speed = _walkSpeed;
            }
        }

        #endregion
    }
}