using UnityEngine;

namespace FPSVNG
{
    public class PlayerHealth : MonoBehaviour
    {
        [SerializeField] private int _maxHealth;

        private int _currentHealth;

        private bool _isDead;

        private void Start()
        {
            _currentHealth = _maxHealth;

            var ui = FindObjectOfType<PlayerUIController>();
            ui.UpdateHealth(_currentHealth, _maxHealth);
        }

        public void ApplyDamage(int damage)
        {
            if (_isDead) return;

            _currentHealth -= damage;

            var ui = FindObjectOfType<PlayerUIController>();
            ui.UpdateHealth(_currentHealth, _maxHealth);
            if (_currentHealth <= 0)
            {
                ui.EndGame();
            }
        }
    }
}