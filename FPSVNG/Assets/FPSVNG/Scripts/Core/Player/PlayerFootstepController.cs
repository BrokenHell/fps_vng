using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FPSVNG
{
    public class PlayerFootstepController : MonoBehaviour
    {
        #region [ Fields ]

        [SerializeField] private AudioSource _soundSource;
        [SerializeField] private List<AudioClip> _footstepClips = new List<AudioClip>();

        [SerializeField] private CharacterController _characterController;
        [SerializeField] private PlayerSprintController _sprintController;
        [SerializeField] private PlayerCrunchController _crunchController;

        [SerializeField] private float _walkStepDistance;
        [SerializeField] private float _sprintStepDistance;
        [SerializeField] private float _crunchStepDistance;

        [SerializeField] private float _walkVolumeMin = 0.3f;
        [SerializeField] private float _walkVolumeMax = 0.7f;

        [SerializeField] private float _sprintVolume = 1f;

        [SerializeField] private float _crunchVolume = 0.1f;

        private float _currentDistance;

        #endregion

        #region [ Unity Lifecycle ]

        private void Start()
        {
            
        }

        private void Update()
        {
            HandleFootstepSound();
        }

        #endregion

        #region [ Private Methods ]

        private void HandleFootstepSound()
        {
            if (!_characterController.isGrounded)
                return;

            if (_characterController.velocity.sqrMagnitude > 0f)
            {
                _currentDistance += Time.deltaTime;

                if (_crunchController.IsCrunch)
                {
                    if (_currentDistance > _crunchStepDistance)
                    {
                        _soundSource.volume = _crunchVolume;
                        _soundSource.clip = _footstepClips[Random.Range(0, _footstepClips.Count)];
                        _soundSource.Play();

                        _currentDistance = 0f;
                    }
                }
                else
                {
                    float stepDistance = _sprintController.IsSprinting ? _sprintStepDistance : _walkStepDistance;

                    if (_currentDistance > stepDistance)
                    {
                        _soundSource.volume = _sprintController.IsSprinting ? _sprintVolume :
                                                                        Random.Range(_walkVolumeMin, _walkVolumeMax);
                        _soundSource.clip = _footstepClips[Random.Range(0, _footstepClips.Count)];
                        _soundSource.Play();

                        _currentDistance = 0f;
                    }
                }
            }
            else
            {
                _currentDistance = 0f;
            }
        }

        #endregion
    }
}