using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace FPSVNG
{
    [System.Serializable]
    public enum EnemyState
    {
        Patrol,
        Chasing,
        Attack,
    }

    public class EnemyController : MonoBehaviour
    {
        #region [ Fields ]

        [SerializeField] private EnemyAnimator _enemyAnimator;
        [SerializeField] private NavMeshAgent _navMeshAgent;

        [SerializeField] private bool _runWhenChase;
        [SerializeField] private float _walkSpeed;
        [SerializeField] private float _runSpeed;

        [SerializeField] private bool _isBoss;

        [SerializeField] private float _chaseDistance = 7f;
        [SerializeField] private float _attackDistance = 1.8f;
        [SerializeField] private float _chaseAfterAttackDistance = 2f;

        [SerializeField] private float _patrolRadiusMin = 20f;
        [SerializeField] private float _patrolRadisMax = 60f;
        [SerializeField] private float _patrolLookingDuration = 15f;

        [SerializeField] private float _waitBeforeAttack = 2f;

        [SerializeField] private int _minDamage;
        [SerializeField] private int _maxDamage;

        private float _patrolTimer;

        private float _currentChaseDistance;

        private float _waitAttackTimer;

        private Transform _target;
        

        public EnemyState EnemyState;

        #endregion

        #region [ Unity lifecycle ]

        private void Start()
        {
            EnemyState = EnemyState.Patrol;
            _patrolTimer = 0f;
            _waitAttackTimer = 0f;

            _currentChaseDistance = _chaseDistance;
            _patrolTimer = _patrolLookingDuration;
        }


        private void Update()
        {
            if (_isBoss)
            {
                HandleBoss();
                return;
            }

            if (EnemyState == EnemyState.Patrol)
            {
                HandlePatrol();
            }
            else if (EnemyState == EnemyState.Chasing)
            {
                HandleChase();
            }
            else if (EnemyState == EnemyState.Attack)
            {
                HandleAttack();
            }
        }

        #endregion

        #region [ Public Methods ]

        public void TargetFinded(Transform target)
        {
            _target = target;
        }

        #endregion

        #region [ Private Methods ]

        private void HandleBoss()
        {
            if (_target != null)
            {
                _navMeshAgent.velocity = Vector3.zero;
                _navMeshAgent.isStopped = false;
                _navMeshAgent.SetDestination(_target.position);

                _waitAttackTimer += Time.deltaTime;
                if (_waitAttackTimer >= _waitBeforeAttack)
                {
                    _enemyAnimator.Attack();

                    _waitAttackTimer -= _waitBeforeAttack;

                    _target.GetComponent<PlayerHealth>().ApplyDamage(Random.Range(_minDamage, _maxDamage));
                }
            }
        }

        private void HandlePatrol()
        {
            _navMeshAgent.isStopped = false;
            _navMeshAgent.speed = _walkSpeed;

            _patrolTimer += Time.deltaTime;

            if (_patrolTimer >= _patrolLookingDuration)
            {
                FindRandomDestination();
                _patrolTimer -= _patrolLookingDuration;
            }

            if (_navMeshAgent.velocity.sqrMagnitude > 0f)
            {
                _enemyAnimator.Walk(true);
            }
            else
            {
                _enemyAnimator.Walk(false);
            }

            if (_target != null)
            {
                _enemyAnimator.Walk(false);
                EnemyState = EnemyState.Chasing;
            }
        }

        private void FindRandomDestination()
        {
            float radius = Random.Range(_patrolRadiusMin, _patrolRadisMax);

            var randDir = Random.insideUnitSphere * radius;
            randDir += transform.position;

            NavMeshHit hit;

            NavMesh.SamplePosition(randDir, out hit, radius, -1);

            _navMeshAgent.SetDestination(hit.position);
        }

        private void HandleChase()
        {
            if (_target == null)
            {
                _patrolTimer = _patrolLookingDuration;
                EnemyState = EnemyState.Patrol;
                return;
            }

            _navMeshAgent.isStopped = false;

            if (_runWhenChase)
            {
                _navMeshAgent.speed = _runSpeed;
                _navMeshAgent.SetDestination(_target.position);

                if (_navMeshAgent.velocity.sqrMagnitude > 0f)
                {
                    _enemyAnimator.Run(true);
                }
                else
                {
                    _enemyAnimator.Run(false);
                }
            }
            else
            {
                _navMeshAgent.speed = _walkSpeed;
                _navMeshAgent.SetDestination(_target.position);

                if (_navMeshAgent.velocity.sqrMagnitude > 0f)
                {
                    _enemyAnimator.Walk(true);
                }
                else
                {
                    _enemyAnimator.Walk(false);
                }
            }

            if (Vector3.Distance(transform.position, _target.position) < _attackDistance)
            {
                _enemyAnimator.Walk(false);
                _enemyAnimator.Run(false);

                EnemyState = EnemyState.Attack;
            }

        }

        private void HandleAttack()
        {
            if (_target == null)
            {
                _patrolTimer = _patrolLookingDuration;
                EnemyState = EnemyState.Patrol;
                return;
            }

            _navMeshAgent.velocity = Vector3.zero;
            _navMeshAgent.isStopped = true;

            _waitAttackTimer += Time.deltaTime;
            if (_waitAttackTimer >= _waitBeforeAttack)
            {
                _enemyAnimator.Attack();

                _waitAttackTimer -= _waitBeforeAttack;

                Debug.LogError("target " + _target.name);

                _target.GetComponent<PlayerHealth>().ApplyDamage(Random.Range(_minDamage, _maxDamage));
            }

            if (Vector3.Distance(transform.position, _target.position) > _attackDistance + _chaseAfterAttackDistance)
            {
                EnemyState = EnemyState.Chasing;
            }
        }

        #endregion
    }
}