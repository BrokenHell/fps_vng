using UnityEngine;

namespace FPSVNG
{
    [RequireComponent(typeof(CharacterController), typeof(PlayerInputHandler))]
    public class PlayerMovementController : MonoBehaviour
    {
        #region [ Fields ]

        //[SerializeField] private float _speed = 5f;
        [SerializeField] private float _gravityForce = 10f;
        [SerializeField] private float _jumpForce = 5f;

        private CharacterController _characterController;
        private PlayerInputHandler _inputHandler;

        private Vector3 _moveDirection;
        private float _jumpVelocity;

        public float Speed { get; set; }

        #endregion

        #region [ Unity Lifecyclce ]

        private void Start()
        {
            _characterController = GetComponent<CharacterController>();
            _inputHandler = GetComponent<PlayerInputHandler>();
        }

        private void Update()
        {
            HandleMovement();
        }

        #endregion

        #region [ Private Methods ]

        private void HandleMovement()
        {
            _moveDirection = _inputHandler.GetMoveInput();
            _moveDirection = transform.TransformDirection(_moveDirection);

            _moveDirection *= Speed * Time.deltaTime;

            ApplyGravity();

            _characterController.Move(_moveDirection);
        }

        private void ApplyGravity()
        {
            _jumpVelocity -= _gravityForce * Time.deltaTime;

            HandleJump();

            _moveDirection.y = _jumpVelocity * Time.deltaTime;
        }

        private void HandleJump()
        {
            if (_characterController.isGrounded && _inputHandler.GetJumpInputDown())
            {
                _jumpVelocity = _jumpForce;
            }
        }

        #endregion

    }
}