using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace FPSVNG
{
    public class EnemyAnimator : MonoBehaviour
    {
        #region [ Fields ]

        [SerializeField] private string attack_state_name = "Zombie_Eating";
        [SerializeField] private string dead_state_name = "Zombie_Die";
        [SerializeField] private Animator _animator;

        #endregion

        #region [ Unity Lifecycle ]

        #endregion

        #region [ Public Methods ]

        public void Walk(bool isWalk)
        {
            _animator.SetBool(AnimationConstants.Walk_Param, isWalk);
        }

        public void Run(bool isRun)
        {
            _animator.SetBool(AnimationConstants.Run_Param, isRun);
        }

        public void Dead()
        {
            _animator.Play(dead_state_name, 0, 0);
            DOVirtual.DelayedCall(1.5f, () =>
            {
                GameObject.Destroy(this.gameObject);
            });
        }

        public void Attack()
        {
            _animator.Play(attack_state_name, 0, 0);
        }

        #endregion
    }
}