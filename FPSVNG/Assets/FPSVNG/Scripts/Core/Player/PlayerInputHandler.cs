using MoreMountains.Tools;
using UnityEngine;
using UnityEngine.UI;

namespace FPSVNG
{
    public class PlayerInputHandler : MonoBehaviour
    {
        #region [ Fields ]

        [Header("Input Axis")]
        [SerializeField] private string _horizontalAxis;
        [SerializeField] private string _verticleAxis;


        [SerializeField] private string _horizontalLookAxis;
        [SerializeField] private string _verticleLookAxis;

        [SerializeField] private string _fireButtonName;
        [SerializeField] private string _jumpButtonName;
        [SerializeField] private string _crunchButtonName;
        [SerializeField] private string _sprintButtonName;
        [SerializeField] private string _aimButtonName;
        [SerializeField] private string _switchWeaponButtonName;

        [Tooltip("Sensitivity multiplier for moving the camera around")]
        [SerializeField] private float _lookSensitivity = 1f;
        [SerializeField] private float _mobileLookSensitivity = 1f;

        [SerializeField] private bool _enableMobile;

        [Header("Mobile")]
        [SerializeField] private MMTouchRepositionableJoystick _joytick;
        [SerializeField] private MMTouchRepositionableJoystick _lookstick;
        [SerializeField] private MMTouchButton _fireButton;
        [SerializeField] private MMTouchButton _jumpButton;
        [SerializeField] private MMTouchButton _crunchButton;
        [SerializeField] private MMTouchButton _aimButton;
        [SerializeField] private MMTouchButton _sprintButton;

        [SerializeField] private Image _gunIcon;

        [SerializeField] private Sprite _riffleIcon;
        [SerializeField] private Sprite _handGunIcon;

        private bool _isMobileAim;
        private int _weaponIndex = 1;
        private int _currentWeaponIndex = 0;
        private bool _isFireButtonDown;

        #endregion

        #region [ Unity Lifecycles ]

        private void Start()
        {
            if (!_enableMobile)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }

        #endregion

        #region [ Public Methods ]

        public void ToggleAim()
        {
            _isMobileAim = !_isMobileAim;
        }

        public void ToggleSwitchWeapon()
        {
            _isMobileAim = false;
            _weaponIndex += 1;
            if (_weaponIndex > 2)
            {
                _weaponIndex = 1;
            }

            if (_weaponIndex == 1)
            {
                _gunIcon.sprite = _handGunIcon;
            }
            else
            {
                _gunIcon.sprite = _riffleIcon;
            }

            _currentWeaponIndex = _weaponIndex;
        }

        public bool CanProcessInput()
        {
            if (_enableMobile) 
                return true;
            return Cursor.lockState == CursorLockMode.Locked;
        }

        public Vector3 GetMoveInput()
        {
            if (CanProcessInput())
            {
                if (_enableMobile)
                {
                    Vector3 move = new Vector3(_joytick._joystickValue.x, 0f, _joytick._joystickValue.y);

                    // constrain move input to a maximum magnitude of 1, otherwise diagonal movement might exceed the max move speed defined
                    move = Vector3.ClampMagnitude(move, 1);

                    return move;
                }
                else
                {
                    Vector3 move = new Vector3(Input.GetAxisRaw(_horizontalAxis), 0f, Input.GetAxisRaw(_verticleAxis));

                    //    // constrain move input to a maximum magnitude of 1, otherwise diagonal movement might exceed the max move speed defined
                    move = Vector3.ClampMagnitude(move, 1);

                    return move;
                }
            }

            return Vector3.zero;
        }

        public float GetLookInputsHorizontal()
        {
            return GetMouseLookAxis(_horizontalLookAxis);
        }

        public float GetLookInputsVertical()
        {
            return GetMouseLookAxis(_verticleLookAxis);
        }

        public bool GetJumpInputDown()
        {
            if (CanProcessInput())
            {
                if (_enableMobile)
                {
                    return _jumpButton.CurrentState == MMTouchButton.ButtonStates.ButtonDown;
                }
                else
                {
                    return Input.GetButton(_jumpButtonName);
                }
            }

            return false;
        }

        public bool GetJumpInputHeld()
        {
            if (CanProcessInput())
            {
                return Input.GetButton(_jumpButtonName);
            }

            return false;
        }

        public bool GetFireInputDown()
        {
            return GetFireInputHeld() && !_isFireButtonDown;
        }

        public bool GetFireInputReleased()
        {
            return !GetFireInputHeld() && _isFireButtonDown;
        }

        public bool GetFireInputHeld()
        {
            if (CanProcessInput())
            {
                if (_enableMobile)
                {
                    return _fireButton.CurrentState == MMTouchButton.ButtonStates.ButtonPressed;
                }
                else
                {
                    return Input.GetButton(_fireButtonName);
                }
            }

            return false;
        }

        public bool GetAimInputHeld()
        {
            if (CanProcessInput())
            {
                if (_enableMobile)
                {
                    return _isMobileAim;
                }

                bool i =  Input.GetButton(_aimButtonName);
                return i;
            }

            return false;
        }

        public bool GetSprintInputHeld()
        {
            if (CanProcessInput())
            {
                if (_enableMobile)
                {
                    return _sprintButton.CurrentState == MMTouchButton.ButtonStates.ButtonPressed;
                }

                return Input.GetButton(_sprintButtonName);
            }

            return false;
        }

        public bool GetCrouchInputDown()
        {
            if (CanProcessInput())
            {
                if (_enableMobile)
                {
                    return _crunchButton.CurrentState == MMTouchButton.ButtonStates.ButtonDown;
                }

                return Input.GetButtonDown(_crunchButtonName);
            }

            return false;
        }

        public bool GetCrouchInputReleased()
        {
            if (CanProcessInput())
            {
                return Input.GetButtonUp(_crunchButtonName);
            }

            return false;
        }

        public int GetSwitchWeaponInput()
        {
            if (CanProcessInput())
            {
                string axisName = _switchWeaponButtonName;

                if (Input.GetAxis(axisName) > 0f)
                    return -1;
                else if (Input.GetAxis(axisName) < 0f)
                    return 1;
            }

            return 0;
        }

        public int GetSelectWeaponInput()
        {
            if (CanProcessInput())
            {
                if (_enableMobile)
                {
                    var i = _currentWeaponIndex;
                    _currentWeaponIndex = 0;

                    return i;
                }

                if (Input.GetKeyDown(KeyCode.Alpha1))
                    return 1;
                else if (Input.GetKeyDown(KeyCode.Alpha2))
                    return 2;
                else if (Input.GetKeyDown(KeyCode.Alpha3))
                    return 3;
                else if (Input.GetKeyDown(KeyCode.Alpha4))
                    return 4;
                else if (Input.GetKeyDown(KeyCode.Alpha5))
                    return 5;
                else if (Input.GetKeyDown(KeyCode.Alpha6))
                    return 6;
                else
                    return 0;
            }

            return 0;
        }

        #endregion

        #region [ Private Methods ]

        private float GetMouseLookAxis(string mouseInputName)
        {
            if (CanProcessInput())
            {
                if (_enableMobile)
                {
                    float i = 0f;

                    // Check if this look input is coming from the mouse
                    if (mouseInputName == _horizontalLookAxis)
                    {
                        i = _lookstick._joystickValue.x;
                    }
                    else if (mouseInputName == _verticleLookAxis)
                    {
                        i = -_lookstick._joystickValue.y;
                    }

                    i *= _mobileLookSensitivity;
                    // reduce mouse input amount to be equivalent to stick movement
                    i *= 0.01f;

                    return i;
                }
                else
                {
                    // Check if this look input is coming from the mouse
                    float i = Input.GetAxisRaw(mouseInputName);

                    // apply sensitivity multiplier
                    i *= _lookSensitivity;

                    // reduce mouse input amount to be equivalent to stick movement
                    i *= 0.01f;

                    return i;
                }
            }

            return 0f;
        }

        #endregion
    }
}