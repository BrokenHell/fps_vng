namespace FPSVNG
{
    public static class AnimationConstants
    {
        public const string Shoot_State = "Fire";


        public const string Aim_Shoot_State = "Aim Fire";

        public const string Walk_Param = "Walk";
        public const string Run_Param = "Run";

        public const string Aim_Param = "Aim";

        public const string Reload_State = "Reload Out Of Ammo";
        public const string Reload_Left_State = "Reload Ammo Left";

        public const string Zombie_Eat_State = "Zombie_Eating";
    }
}