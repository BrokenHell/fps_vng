using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FPSVNG
{
    [System.Serializable]
    public enum AimType
    {
        None,
        Aim
    }

    [System.Serializable]
    public enum WeaponFireType
    {
        Single,
        Multi
    }

    [System.Serializable]
    public enum BulletType
    {
        Gun
    }

    public class WeaponHandler : MonoBehaviour
    {
        #region [ Fields ]

        [SerializeField] private Animator _animator;
        [SerializeField] private GameObject _muzzleFlash;
        [SerializeField] private AudioSource _shootSound;
        [SerializeField] private AudioSource _reloadSound;
        [SerializeField] private GameObject _attackPoint;
        [SerializeField] private float _fireRate;

        [SerializeField] private Transform _bulletPrefab;
        [SerializeField] private Transform _bulletSpawnPoint;
        [SerializeField] private float _bulletForce;

        [SerializeField] private Transform _casingPrefab;
        [SerializeField] private Transform _casingSpawnPoint;

        [SerializeField] private int _minDamage;
        [SerializeField] private int _maxDamage;

        public AimType aimType;
        public WeaponFireType fireType;
        public BulletType bulletType;

        public float FireRate => _fireRate;

        public int MinDamage => _minDamage;
        public int MaxDamage => _maxDamage;

        #endregion

        #region [ Unity Lifecycle ]


        #endregion

        #region [ Public Methods ]

        public void ShootAnimation(bool isAim, int damage)
        {
            if (isAim)
            {
                _animator.Play(AnimationConstants.Aim_Shoot_State, 0, 0f);
            }
            else
            {
                _animator.Play(AnimationConstants.Shoot_State, 0, 0f);
            }

            //Spawn bullet from bullet spawnpoint
            var bullet = (Transform)Instantiate(
                _bulletPrefab,
                _bulletSpawnPoint.position,
                _bulletSpawnPoint.rotation);

            //Add velocity to the bullet
            bullet.GetComponent<Rigidbody>().velocity =
                bullet.transform.forward * _bulletForce;

            var bulletScript = bullet.GetComponent<BulletScript>();
            bulletScript.source = this.transform;
            bulletScript.damage = damage;

            //Spawn casing prefab at spawnpoint
            Instantiate(_casingPrefab,
                _casingSpawnPoint.position,
                _casingSpawnPoint.rotation);
        }

        public void Aim(bool isAim)
        {
            aimType = isAim ? AimType.Aim : AimType.None;
            _animator.SetBool(AnimationConstants.Aim_Param, isAim);
        }

        #endregion

        #region [ Private Methods ]

        private void SetActiveMuzzleFlash(bool isActive)
        {
            _muzzleFlash.SetActive(isActive);
        }

        private void SetActiveAttackPoint(bool isActive)
        {
            _attackPoint.SetActive(isActive);
        }

        private void PlayShootSound()
        {
            _shootSound.Play();
        }

        private void PlayReloadSound()
        {
            _reloadSound.Play();
        }

        #endregion
    }
}