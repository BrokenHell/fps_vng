using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FPSVNG
{
    public class EnemyEyeController : MonoBehaviour
    {
        [SerializeField] private EnemyController _controller;

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                Debug.LogError("Target find : " + other.gameObject.transform.name);
                _controller.TargetFinded(other.gameObject.transform);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                _controller.TargetFinded(null);
            }
        }
    }
}