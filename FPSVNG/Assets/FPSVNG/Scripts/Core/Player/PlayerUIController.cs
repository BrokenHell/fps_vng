using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace FPSVNG
{
    public class PlayerUIController : MonoBehaviour
    {
        #region [ Fields ]

        [SerializeField] private TextMeshProUGUI _labelZombieKilled;

        [SerializeField] private TextMeshProUGUI _labelPlayerHealth;
        [SerializeField] private Image _healthBar;

        [SerializeField] private GameObject _endGameUI;
        [SerializeField] private int _requireKill = 10;

        private int _killCount = 0;

        #endregion

        private void Start()
        {
            _labelZombieKilled.text = string.Format("Zombie Killed\n{0}/{1}", _killCount, _requireKill);
        }

        public void ResetGame()
        {
            SceneManager.LoadScene(0);
        }

        public void EndGame()
        {
            DOVirtual.DelayedCall(1f, () =>
            {
                _endGameUI.SetActive(true);
            });
        }

        public void OnKillZombie()
        {
            _killCount += 1;
            _labelZombieKilled.text = string.Format("Zombie Killed\n{0}/{1}", _killCount, _requireKill);

            if (_killCount >= _requireKill)
            {
                DOVirtual.DelayedCall(1f, () =>
                {
                    _endGameUI.SetActive(true);
                });
            }
        }

        public void UpdateHealth(int current, int max)
        {
            _labelPlayerHealth.text = string.Format("{0}/{1}", current, max);
            _healthBar.fillAmount = (float)current / (float)max;
        }
    }
}