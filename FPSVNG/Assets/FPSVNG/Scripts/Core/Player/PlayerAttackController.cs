using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FPSVNG
{
    [RequireComponent(typeof(PlayerInputHandler))]
    public class PlayerAttackController : MonoBehaviour
    {
        #region [ Fields ]

        [SerializeField] private WeaponManager _weaponManager;
        [SerializeField] private Camera _fpCamera;

        [SerializeField] private GameObject _crossHair;

        [SerializeField] private float _zoomInCameraFov;
        [SerializeField] private float _zoomOutCameraFov;
        [SerializeField] private float _zoomSpeed;

        private PlayerInputHandler _inputHandler;

        private bool _isAim;

        private float _currentTime;

        public bool IsAim => _isAim;

        #endregion

        #region [ Unity Lifecycle ]

        private void Start()
        {
            _inputHandler = GetComponent<PlayerInputHandler>();
        }

        private void Update()
        {
            HandleZoom();
            _currentTime += Time.deltaTime;
            HandleAttack();
        }

        #endregion

        #region [ Private Methods ]

        private void HandleAttack()
        {
            var weapon = _weaponManager.CurrentSelected;

            if (weapon != null)
            {
                if (weapon.fireType == WeaponFireType.Multi)
                {
                    if (_inputHandler.GetFireInputHeld() && _currentTime > (1f / weapon.FireRate))
                    {
                        _currentTime = 0f;
                        weapon.ShootAnimation(_isAim, Random.Range(weapon.MinDamage, weapon.MaxDamage));
                    }
                }
            }
        }

        private void HandleZoom()
        {
            var weapon = _weaponManager.CurrentSelected;
            if (weapon != null)
            {
                if (_inputHandler.GetAimInputHeld())
                {
                    if (!_isAim)
                    {
                        _isAim = true;
                        //Start aiming
                        weapon.Aim(true);

                        _crossHair.SetActive(false);
                    }

                    //When right click is released
                    _fpCamera.fieldOfView = Mathf.Lerp(_fpCamera.fieldOfView,
                        _zoomInCameraFov, _zoomSpeed * Time.deltaTime);

                }
                else
                {
                    if (_isAim)
                    {
                        _isAim = false;
                        //Start aiming
                        weapon.Aim(false);

                        _crossHair.SetActive(true);
                    }

                    //When right click is released
                    _fpCamera.fieldOfView = Mathf.Lerp(_fpCamera.fieldOfView,
                        _zoomOutCameraFov, _zoomSpeed * Time.deltaTime);
                }
            }
        }

        #endregion
    }
}