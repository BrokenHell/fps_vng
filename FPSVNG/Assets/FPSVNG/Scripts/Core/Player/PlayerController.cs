using UnityEngine;
using UnityEngine.Events;

namespace FPSVNG
{
    [System.Serializable]
    public class PlayerStanceChangedEvent : UnityEvent<bool> { }

    [RequireComponent(typeof(CharacterController),typeof(PlayerInputHandler))]
    public class PlayerController : MonoBehaviour
    {
        #region [ Fields ]

        [SerializeField] private Camera _fpCamera;
        [SerializeField] private LayerMask _groundCheckLayer = -1;
        [SerializeField] private float _groundCheckDistance = 0.05f;

        [Tooltip("Force applied downward when in the air")]
        [SerializeField] private float _gravityForce = 20f;

        [Header("Movement")]
        [Tooltip("Maximum movement speed when on ground")]
        [SerializeField] private float _maxGroundSpeed = 10f;
        [SerializeField] private float _movementSpeed = 15f;
        [SerializeField] private float _maxSprintSpeed = 10f;
        [Range(0.1f, 1f)]
        [SerializeField] private float _crunchSpeedMultiply = 0.5f;

        [SerializeField] private float _sprintMultiply = 2f;

        [SerializeField] private float _inAirMovementSpeed = 25f;
        [SerializeField] private float _inAirMaxSpeed = 10f;

        [Tooltip("Height at which the player die instantly when falling off the map")]
        [SerializeField] private float _killHeight = -50f;

        [Header("Rotation")]
        [SerializeField] private float _rotationSpeed = 200f;
        [SerializeField] private float _aimRotationMultiply = 0.4f;

        [Header("Jump")]
        [SerializeField] private float _jumpForce = 10f;
        [SerializeField] private float _jumpGroundingPreventionTime = 0.2f;

        [Header("Stance")]
        [SerializeField] private float _cameraHeightRatio = 0.9f;
        [SerializeField] private float _standColliderHeight = 1.8f;
        [SerializeField] private float _crounchColliderHeight = 0.9f;
        [SerializeField] private float _crounchSpeed = 10f;

        [Header("Events")]
        [SerializeField] private PlayerStanceChangedEvent StanceChanged;

        private CharacterController _characterController;
        private PlayerInputHandler _inputHandler;

        private float _groundInAirCheckDistance = 0.07f;

        private bool _isDead;
        [SerializeField]
        private bool _isGrounded;

        private Vector3 _groundNormal;

        private float _lastJumpTime;

        private float _cameraVerticleAngle;

        private float _targetCharacterHeight;

        private bool _isCrunch;

        [SerializeField]
        private Vector3 _characterVelocity;

        private Vector3 _lastImpactSpeed;

        private bool _hasJumpThisFrame;

        public float RotationMultiplier
        {
            get
            {
                //if (m_WeaponsManager.isAiming)
                //{
                    //return _aimRotationMultiply;
                //}

                return 1f;
            }
        }


        #endregion

        #region [ Unity Lifecycles ]

        private void Start()
        {
            _characterController = GetComponent<CharacterController>();
            _inputHandler = GetComponent<PlayerInputHandler>();

            _characterController.enableOverlapRecovery = true;

            // force the crouch state to false when starting
            SetCrouchingState(false, true);
            UpdateCharacterHeight(true);
        }

        private void Update()
        {
            // check for Y kill
            if (!_isDead && transform.position.y < _killHeight)
            {
                //m_Health.Kill();
            }

            _hasJumpThisFrame = false;

            GroundCheck();

            // crouching
            if (_inputHandler.GetCrouchInputDown())
            {
                SetCrouchingState(!_isCrunch, false);
            }

            UpdateCharacterHeight(false);

            HandleCharacterMovement();
        }

        #endregion

        #region [ Private Methods ]

        void HandleCharacterMovement()
        {
            {

                // rotate the transform with the input speed around its local Y axis
                transform.Rotate(new Vector3(0f, (_inputHandler.GetLookInputsHorizontal() * _rotationSpeed * RotationMultiplier), 0f), Space.Self);
            }

            {
                // add vertical inputs to the camera's vertical angle
                _cameraVerticleAngle += _inputHandler.GetLookInputsVertical() * _rotationSpeed * RotationMultiplier;

                // limit the camera's vertical angle to min/max
                _cameraVerticleAngle = Mathf.Clamp(_cameraVerticleAngle, -89f, 89f);

                // apply the vertical angle as a local rotation to the camera transform along its right axis (makes it pivot up and down)
                _fpCamera.transform.localEulerAngles = new Vector3(_cameraVerticleAngle, 0, 0);

            }

            // character movement handling
            bool isSprinting = _inputHandler.GetSprintInputHeld();
            {
                if (isSprinting)
                {
                    isSprinting = SetCrouchingState(false, false);
                }

                float speedModifier = isSprinting ? _sprintMultiply : 1f;

                // converts move input to a worldspace vector based on our character's transform orientation
                Vector3 worldspaceMoveInput = transform.TransformVector(_inputHandler.GetMoveInput());

                // handle grounded movement
                if (_isGrounded)
                {
                    // calculate the desired velocity from inputs, max speed, and current slope
                    Vector3 targetVelocity = worldspaceMoveInput * _maxGroundSpeed * speedModifier;
                    // reduce speed if crouching by crouch speed ratio
                    if (_isCrunch)
                        targetVelocity *= _crunchSpeedMultiply;

                    targetVelocity = GetDirectionReorientedOnSlope(targetVelocity.normalized, _groundNormal) * targetVelocity.magnitude;

                    // smoothly interpolate between our current velocity and the target velocity based on acceleration speed
                    _characterVelocity = Vector3.Lerp(_characterVelocity, targetVelocity, _movementSpeed * Time.deltaTime);

                    Debug.LogError("targetVelocity " + targetVelocity);
                    Debug.LogError("_characterVelocity " + _characterVelocity);

                    // jumping
                    if (_isGrounded && _inputHandler.GetJumpInputDown())
                    {
                        // force the crouch state to false
                        if (SetCrouchingState(false, false))
                        {
                            // start by canceling out the vertical component of our velocity
                            _characterVelocity = new Vector3(_characterVelocity.x, 0f, _characterVelocity.z);

                            // then, add the jumpSpeed value upwards
                            _characterVelocity += Vector3.up * _jumpForce;

                            // remember last time we jumped because we need to prevent snapping to ground for a short time
                            _lastJumpTime = Time.time;
                            _hasJumpThisFrame = true;

                            // Force grounding to false
                            _isGrounded = false;
                            _groundNormal = Vector3.up;
                        }
                    }

                    // footsteps sound
                    //float chosenFootstepSFXFrequency = (isSprinting ? footstepSFXFrequencyWhileSprinting : footstepSFXFrequency);
                    //if (m_footstepDistanceCounter >= 1f / chosenFootstepSFXFrequency)
                    //{
                    //    m_footstepDistanceCounter = 0f;
                    //    audioSource.PlayOneShot(footstepSFX);
                    //}

                    //// keep track of distance traveled for footsteps sound
                    //m_footstepDistanceCounter += characterVelocity.magnitude * Time.deltaTime;
                }
                // handle air movement
                else
                {
                    // add air acceleration
                    _characterVelocity += worldspaceMoveInput * _inAirMovementSpeed * Time.deltaTime;

                    // limit air speed to a maximum, but only horizontally
                    float verticalVelocity = _characterVelocity.y;
                    Vector3 horizontalVelocity = Vector3.ProjectOnPlane(_characterVelocity, Vector3.up);
                    horizontalVelocity = Vector3.ClampMagnitude(horizontalVelocity, _inAirMaxSpeed * speedModifier);
                    _characterVelocity = horizontalVelocity + (Vector3.up * verticalVelocity);

                    // apply the gravity to the velocity
                    _characterVelocity += Vector3.down * _gravityForce * Time.deltaTime;
                }
            }

            // apply the final calculated velocity value as a character movement
            Vector3 capsuleBottomBeforeMove = GetCapsuleBottomHemisphere();
            Vector3 capsuleTopBeforeMove = GetCapsuleTopHemisphere(_characterController.height);
            _characterController.Move(_characterVelocity * Time.deltaTime);

            // detect obstructions to adjust velocity accordingly
            _lastImpactSpeed = Vector3.zero;
            if (Physics.CapsuleCast(capsuleBottomBeforeMove, capsuleTopBeforeMove, _characterController.radius,
                _characterVelocity.normalized, out RaycastHit hit,
                _characterVelocity.magnitude * Time.deltaTime,
                -1,
                QueryTriggerInteraction.Ignore))
            {
                // We remember the last impact speed because the fall damage logic might need it
                _lastImpactSpeed = _characterVelocity;

                _characterVelocity = Vector3.ProjectOnPlane(_characterVelocity, hit.normal);
            }
        }

        // returns false if there was an obstruction
        private bool SetCrouchingState(bool crouched, bool ignoreObstructions)
        {
            // set appropriate heights
            if (crouched)
            {
                _targetCharacterHeight = _crounchColliderHeight;
            }
            else
            {
                // Detect obstructions
                if (!ignoreObstructions)
                {
                    Collider[] standingOverlaps = Physics.OverlapCapsule(
                        GetCapsuleBottomHemisphere(),
                        GetCapsuleTopHemisphere(_standColliderHeight),
                        _characterController.radius,
                        -1,
                        QueryTriggerInteraction.Ignore);
                    foreach (Collider c in standingOverlaps)
                    {
                        if (c != _characterController)
                        {
                            return false;
                        }
                    }
                }

                _targetCharacterHeight = _standColliderHeight;
            }

            StanceChanged?.Invoke(crouched);

            _isCrunch = crouched;
            return true;
        }

        private void UpdateCharacterHeight(bool instant)
        {
            // Update height instantly
            if (instant)
            {
                _characterController.height = _targetCharacterHeight;
                _characterController.center = Vector3.up * _characterController.height * 0.5f;
                _fpCamera.transform.localPosition = Vector3.up * _targetCharacterHeight * _cameraHeightRatio;
                //m_Actor.aimPoint.transform.localPosition = m_Controller.center;
            }
            // Update smooth height
            else if (_characterController.height != _targetCharacterHeight)
            {
                // resize the capsule and adjust camera position
                _characterController.height = Mathf.Lerp(_characterController.height, _targetCharacterHeight, _crounchSpeed * Time.deltaTime);
                _characterController.center = Vector3.up * _characterController.height * 0.5f;
                _fpCamera.transform.localPosition = Vector3.Lerp(_fpCamera.transform.localPosition, Vector3.up * _targetCharacterHeight * _cameraHeightRatio, _crounchSpeed * Time.deltaTime);
                //m_Actor.aimPoint.transform.localPosition = m_Controller.center;
            }
        }

        private void GroundCheck()
        {
            float chosenGroundCheckDistance = _isGrounded ? (_characterController.skinWidth + _groundCheckDistance) : _groundInAirCheckDistance;

            _isGrounded = false;
            _groundNormal = Vector3.up;

            if (Time.time >= _lastJumpTime + _jumpGroundingPreventionTime)
            {
                // if we're grounded, collect info about the ground normal with a downward capsule cast representing our character capsule
                if (Physics.CapsuleCast(GetCapsuleBottomHemisphere(), GetCapsuleTopHemisphere(_characterController.height), _characterController.radius, Vector3.down, out RaycastHit hit, chosenGroundCheckDistance, _groundCheckLayer, QueryTriggerInteraction.Ignore))
                {
                    // storing the upward direction for the surface found
                    _groundNormal = hit.normal;

                    // Only consider this a valid ground hit if the ground normal goes in the same direction as the character up
                    // and if the slope angle is lower than the character controller's limit
                    if (Vector3.Dot(hit.normal, transform.up) > 0f &&
                        IsNormalUnderSlopeLimit(_groundNormal))
                    {
                        _isGrounded = true;

                        // handle snapping to the ground
                        if (hit.distance > _characterController.skinWidth)
                        {
                            _characterController.Move(Vector3.down * hit.distance);
                        }
                    }
                }
            }
        }

        bool IsNormalUnderSlopeLimit(Vector3 normal)
        {
            return Vector3.Angle(transform.up, normal) <= _characterController.slopeLimit;
        }

        Vector3 GetCapsuleBottomHemisphere()
        {
            return transform.position + (transform.up * _characterController.radius);
        }

          
        Vector3 GetCapsuleTopHemisphere(float atHeight)
        {
            return transform.position + (transform.up * (atHeight - _characterController.radius));
        }

        public Vector3 GetDirectionReorientedOnSlope(Vector3 direction, Vector3 slopeNormal)
        {
            Vector3 directionRight = Vector3.Cross(direction, transform.up);
            return Vector3.Cross(slopeNormal, directionRight).normalized;
        }

        #endregion
    }
}