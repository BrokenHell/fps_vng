using UnityEngine;

namespace FPSVNG
{
    [RequireComponent(typeof(PlayerInputHandler), typeof(CharacterController))]
    public class PlayerCrunchController : MonoBehaviour
    {
        #region [ Fields ]

        [SerializeField] private float _standHeight = 1.8f;
        [SerializeField] private float _crunchHeight = 0.9f;
        [SerializeField] private float _crunchSpeed = 10f;

        [SerializeField] private Transform _lookRoot;

        [SerializeField] private bool _isCrunch;

        private CharacterController _characterController;
        private PlayerInputHandler _inputHandler;

        private float _targetHeight;

        public bool IsCrunch => _isCrunch;

        #endregion

        #region [ Unity Lifecycle ]

        private void Start()
        {
            _characterController = GetComponent<CharacterController>();
            _inputHandler = GetComponent<PlayerInputHandler>();

            _targetHeight = _standHeight;

            UpdateHeight(true);
        }

        private void Update()
        {
            HandleCrunch();

            UpdateHeight(false);
        }

        #endregion

        #region [ Private Methods ]

        private void UpdateHeight(bool instant)
        {
            if (instant)
            {
                _characterController.height = _targetHeight;
                _characterController.center = Vector3.up * _characterController.height * 0.5f;
                _lookRoot.localPosition = new Vector3(0, _targetHeight, 0);
            }
            else if (_characterController.height != _targetHeight)
            {
                _characterController.height = Mathf.Lerp(_characterController.height, _targetHeight, _crunchSpeed * Time.deltaTime);
                _characterController.center = Vector3.up * _characterController.height * 0.5f;
                _lookRoot.localPosition = Vector3.Lerp(_lookRoot.localPosition, Vector3.up * _targetHeight, _crunchSpeed * Time.deltaTime);
            }
        }

        private void HandleCrunch()
        {
            if (_inputHandler.GetCrouchInputDown())
            {
                _isCrunch = !_isCrunch;

                _targetHeight = _isCrunch ? _crunchHeight : _standHeight;
            }
        }

        #endregion
    }
}