using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

namespace FPSVNG
{

    public class Health : MonoBehaviour
    {
        #region [ Fields ]

        [SerializeField] private EnemyAnimator _animator;
        [SerializeField] private NavMeshAgent _navMeshAgent;
        [SerializeField] private EnemyController _controller;

        [SerializeField] private float _maxHealth = 100f;

        [SerializeField] private float _currentHp = 0;

        [Header("UI")]
        [SerializeField] private TextMeshProUGUI _labelDamagePrefab;

        private bool _isDead;

        public bool IsDead => _isDead;

        #endregion

        #region [ Unity Lifecycle ]

        private void Start()
        {
            _currentHp = _maxHealth;
        }

        #endregion

        #region [ Public Methods ]

        public void ApplyDamage(Transform source, int damage)
        {
            if (_isDead)
                return;

            //_controller.TargetFinded(source);

            _currentHp -= damage;

            //var label = GameObject.Instantiate(_labelDamagePrefab, _labelDamagePrefab.transform.parent);
            //label.gameObject.SetActive(true);
            //label.text = damage.ToString();
            //label.transform.localPosition = new Vector3(Random.Range(-1, 0.1f), Random.Range(1, 2), -1);

            //label.DOFade(0, 0.5f).OnComplete(() =>
            //{
            //    GameObject.Destroy(label.gameObject);
            //});


            if (_currentHp <= 0)
            {
                _isDead = true;
                _animator.Dead();

                var uiController = FindObjectOfType<PlayerUIController>();
                if (uiController != null)
                {
                    uiController.OnKillZombie();
                }
            }
        }

        #endregion
    }
}