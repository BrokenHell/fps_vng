using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FPSVNG
{
    [RequireComponent(typeof(PlayerInputHandler))]
    public class PlayerLookController : MonoBehaviour
    {
        #region [ Fields ]

        [SerializeField] private Transform _playerRoot;
        [SerializeField] private Transform _lookRoot;

        [SerializeField] private float _rotationSpeed = 200f;

        [SerializeField] private Vector2 _verticleLookLimit = new Vector2(-90f, 90f);

        private PlayerInputHandler _inputHandler;

        private float _verticalLookAngle;

        public float RotationMultiplier
        {
            get
            {
                return 1f;
            }
        }

        #endregion

        #region [ Unity Lifecycle ]

        private void Start()
        {
            _inputHandler = GetComponent<PlayerInputHandler>();
        }

        private void Update()
        {
            UpdateRotateHorizontal();
            UpdateRotationVertical();
        }

        #endregion

        #region [ Private Methods ]

        private void UpdateRotateHorizontal()
        {
            transform.Rotate(new Vector3(0f, (_inputHandler.GetLookInputsHorizontal() * _rotationSpeed * RotationMultiplier), 0f), Space.Self);
        }

        private void UpdateRotationVertical()
        {
            // add vertical inputs to the camera's vertical angle
            _verticalLookAngle += _inputHandler.GetLookInputsVertical() * _rotationSpeed * RotationMultiplier;

            // limit the camera's vertical angle to min/max
            _verticalLookAngle = Mathf.Clamp(_verticalLookAngle, _verticleLookLimit.x, _verticleLookLimit.y);

            // apply the vertical angle as a local rotation to the camera transform along its right axis (makes it pivot up and down)
            _lookRoot.transform.localEulerAngles = new Vector3(_verticalLookAngle, 0, 0);
        }

        #endregion
    }
}